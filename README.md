### Supervisor

The idea is to create a “daemon supervisor”. This wrapper will monitor the process is running at all times and it will try to restart it in case it's down.

```sh
➜ python supervisor.py --help
usage: supervisor.py [-h] [--check CHECK] [--sleep SLEEP] [--retry RETRY]
                     command

Supervise running process. Restart if needed

positional arguments:
  command               Command/Process to supervise.

optional arguments:
  -h, --help            show this help message and exit
  --check CHECK, -c CHECK
                        Check process is running every x seconds. (default: 3)
  --sleep SLEEP, -s SLEEP
                        Seconds to wait between attempts to restart service.
                        (default: 3)
  --retry RETRY, -r RETRY
                        Attempts to restart the process. (default: 3)
```

#### Examples
```sh
# Note that it doesn't care the process exited with code 0. It will still try to restart it.
➜ python supervisor.py 'bash -c "sleep 1 && exit 0"'
2020-06-13 18:53:44,811 INFO Running command: `bash -c "sleep 1 && exit 0"`
2020-06-13 18:53:47,819 WARNING Process exit with code `0` and output ``
2020-06-13 18:53:47,819 INFO Attempting restart [1]...
2020-06-13 18:53:50,826 WARNING Process exit with code `0` and output ``
2020-06-13 18:53:50,826 INFO Attempting restart [2]...
2020-06-13 18:53:53,834 WARNING Process exit with code `0` and output ``
2020-06-13 18:53:53,835 INFO Attempting restart [3]...
2020-06-13 18:53:56,838 WARNING Process exit with code `0` and output ``
2020-06-13 18:53:56,839 ERROR I am sorry. Giving up after trying to restart the process 3 times
```

```sh
➜ python supervisor.py 'bash -c "sleep 5 && exit 0"'
2020-06-13 18:55:12,526 INFO Running command: `bash -c "sleep 5 && exit 0"`
2020-06-13 18:55:15,532 INFO Process running!
2020-06-13 18:55:18,536 WARNING Process exit with code `0` and output ``
2020-06-13 18:55:18,536 INFO Attempting restart [1]...
2020-06-13 18:55:21,544 INFO Process running!
2020-06-13 18:55:24,545 WARNING Process exit with code `0` and output ``
2020-06-13 18:55:24,545 INFO Attempting restart [2]...
2020-06-13 18:55:27,551 INFO Process running!
2020-06-13 18:55:30,552 WARNING Process exit with code `0` and output ``
2020-06-13 18:55:30,552 INFO Attempting restart [3]...
2020-06-13 18:55:33,557 INFO Process running!
2020-06-13 18:55:36,563 WARNING Process exit with code `0` and output ``
2020-06-13 18:55:36,563 ERROR I am sorry. Giving up after trying to restart the process 3 times
```

```sh
➜ python supervisor.py 'bash -c "sleep 1 && exit 1"'
2020-06-13 18:56:02,859 INFO Running command: `bash -c "sleep 1 && exit 1"`
2020-06-13 18:56:05,864 WARNING Process exit with code `1` and output ``
2020-06-13 18:56:05,864 INFO Attempting restart [1]...
2020-06-13 18:56:08,872 WARNING Process exit with code `1` and output ``
2020-06-13 18:56:08,872 INFO Attempting restart [2]...
2020-06-13 18:56:11,877 WARNING Process exit with code `1` and output ``
2020-06-13 18:56:11,877 INFO Attempting restart [3]...
2020-06-13 18:56:14,884 WARNING Process exit with code `1` and output ``
2020-06-13 18:56:14,884 ERROR I am sorry. Giving up after trying to restart the process 3 times
```

```sh
➜ python supervisor.py 'sh -c "sleep 10 && exit 1"' 
2020-06-13 18:57:18,043 INFO Running command: `sh -c "sleep 10 && exit 1"`
2020-06-13 18:57:21,049 INFO Process running!
2020-06-13 18:57:30,059 WARNING Process exit with code `1` and output ``
2020-06-13 18:57:30,060 INFO Attempting restart [1]...
2020-06-13 18:57:33,063 INFO Process running!
2020-06-13 18:57:42,073 WARNING Process exit with code `1` and output ``
2020-06-13 18:57:42,074 INFO Attempting restart [2]...
2020-06-13 18:57:45,082 INFO Process running!
2020-06-13 18:57:54,091 WARNING Process exit with code `1` and output ``
2020-06-13 18:57:54,091 INFO Attempting restart [3]...
2020-06-13 18:57:57,098 INFO Process running!
2020-06-13 18:58:06,112 WARNING Process exit with code `1` and output ``
2020-06-13 18:58:06,113 ERROR I am sorry. Giving up after trying to restart the process 3 times
```

```sh
# Note i made sure that the `lock` file was not in the directory before running
➜ python supervisor.py 'bash -c "if [ -f lock ]; then exit 1; fi; sleep 10 && touch lock && exit 1"'
2020-06-13 18:58:36,729 INFO Running command: `bash -c "if [ -f lock ]; then exit 1; fi; sleep 10 && touch lock && exit 1"`
2020-06-13 18:58:39,732 INFO Process running!
2020-06-13 18:58:48,738 WARNING Process exit with code `1` and output ``
2020-06-13 18:58:48,739 INFO Attempting restart [1]...
2020-06-13 18:58:51,744 WARNING Process exit with code `1` and output ``
2020-06-13 18:58:51,745 INFO Attempting restart [2]...
2020-06-13 18:58:54,752 WARNING Process exit with code `1` and output ``
2020-06-13 18:58:54,753 INFO Attempting restart [3]...
2020-06-13 18:58:57,757 WARNING Process exit with code `1` and output ``
2020-06-13 18:58:57,758 ERROR I am sorry. Giving up after trying to restart the process 3 times
```

```sh
# Note i made sure that the `lock` file was not in the directory before running
➜ python supervisor.py -r 1 'sh -c "if [ -f lock ]; then exit 1; fi; sleep 10 && touch lock && exit 1"'
2020-06-13 19:00:38,863 INFO Running command: `sh -c "if [ -f lock ]; then exit 1; fi; sleep 10 && touch lock && exit 1"`
2020-06-13 19:00:41,869 INFO Process running!
2020-06-13 19:00:50,879 WARNING Process exit with code `1` and output ``
2020-06-13 19:00:50,879 INFO Attempting restart [1]...
2020-06-13 19:00:53,885 WARNING Process exit with code `1` and output ``
2020-06-13 19:00:53,886 ERROR I am sorry. Giving up after trying to restart the process 1 times
```
