#!/usr/bin/env python3

import os
import sys
import logging
import argparse
import subprocess
from time import sleep

logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s',
                    level=logging.DEBUG,
                    stream=sys.stdout)


def healthy(proc):
    return bool(proc.poll() is None)

def proc_exit(proc):
    output, errors = proc.communicate()
    logging.warning("Process exit with code `%s` and output `%s`", proc.poll(), output)


def run(cmd, r, s, attempts):
    maxRetry = r + 1 - attempts

    for i in range(1, maxRetry):
        current = attempts + i
        if current > 0:
            logging.info("Attempting restart [%s]...", current)
        proc = subprocess.Popen(cmd, shell=True, universal_newlines=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, env=dict(**os.environ))

        # Sleep until checking if service is up
        sleep(s)
        if healthy(proc):
            logging.info("Process running!")
            attempts = current
            return proc, attempts

        proc_exit(proc)

    # If i get here, then I've exhausted all my reattempts and the app still doesn't start
    logging.error("I am sorry. Giving up after trying to restart the process %s times", r)
    sys.exit(1)

def monitor(params):
    # Initialize
    logging.info("Running command: `%s`", params.command)
    proc, attempts = run(params.command, params.retry, params.sleep, -1)

    # Monitor
    while True:
        sleep(params.check)

        if not healthy(proc):
            proc_exit(proc)
            proc, attempts = run(params.command, params.retry, params.sleep, attempts)



# Configure arguments
parser = argparse.ArgumentParser(description='Supervise running process. Restart if it crashes', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--check', '-c', type=int ,default=3, help="Check process is running every x seconds.")
parser.add_argument('--sleep', '-s', type=int, default=3, help="Seconds to wait between attempts to restart service.")
parser.add_argument('--retry', '-r', type=int, default=3, help="Attempts to restart the process.")
parser.add_argument('command', help="Command/Process to supervise.")
parser.set_defaults(func=monitor)


if __name__ == '__main__':
    args = parser.parse_args()
    args.func(args)
